# Simple Ops Automation: Zabbix, Rundeck, OTRS

This repo is covering simple Ops Automation using Zabbix (monitoring & inventory), Rundeck (execution engine) and OTRS (ticketing system). For bigger environments with hundreds of monitored hosts and possible high volume of emitted events from Zabbix, would be better integrate messaging system between those tools (like RabbitMQ, Kafka, ActiveMQ, ...).  
No programing language is required, I wrote everything in bash, so should be simple also for beginners, but with Zabbix, Rundeck and OTRS knowledge (not covered here).  

## Event flow & Rundeck nodes management

### Event flow

1. Zabbix trigger is activated and executes defined action
2. Zabbix PROBLEM event is sent to postfix server where script is handling incoming mails
3. Mail script starts "Trigger name" Rundeck job, passing values parsed from event subject and message
4. Rundeck job creates OTRS ticket and acknowledge Zabbix event
5. Rundeck job executes required action to fix the problem
6. Zabbix OK event is sent to postfix server where script is handling incoming mails
7. Mail script starts "OK" Rundeck job, passing values parsed from event subject and message
8. Rundeck job resolves/updates OTRS ticket based on defined rules and previous PROBLEM event execution

### Rundeck nodes management

Because I'm to lazy to manage Zabbix agents and also nodes in Rundeck, I'm using script to catch Rundeck nodes from Zabbix database. I used asset tag "AUTO" to enable automation, if not set, node is not visible in Rundeck.

## Prerequisites & versions

### Prerequisites

1. Zabbix, Rundeck and OTRS basic knowledge
2. Zabbix server installed, configured and running
3. Rundeck server installed, configured and running
4. OTRS server installed, configured and running
5. EPEL repo enabled on RHEL/CentOS

### Additional packages

- Zabbix server: procmail, jq, curl  
RHEL/CentOS: `yum -y install procmail jq curl`  
Debian/Ubuntu: `apt-get -y install procmail jq curl`  
- Rundeck server: jq, curl  
RHEL/CentOS: `yum -y install jq curl`  
Debian/Ubuntu: `apt-get -y install jq curl`  

### Versions tested

- OS: CentOS 7.5.1804
- Zabbix server: 3.4.10
- Rundeck server: 2.11.4
- OTRS server: 6.0.8

## Setup
### OTRS server

1. Create new queue (if you choose different name, update scripts): `Automation`
2. Create new customer:
  - Customer ID: `AUTO`
  - Customer name: `Automation`
3. Create customer user (if you choose different username, update scripts & Rundeck jobs): `rundeck@automation.local`
4. Import Web Service from following file and set name to REST (if you choose different name, update scripts): [REST.yml](otrs/REST.yml)

### Rundeck server

1. Follow instructions here: [rundeck-automation-example](https://gitlab.com/mhaluska//rundeck-automation-example)
2. Copy scripts from `rundeck` folder to `/var/rundeck/scripts/automation` (if you choose different folder, update Rundeck jobs & node script path)
3. Setup nodes in project, you need two sources, one xml file for local Rundeck server node and one script for nodes from Zabbix
 - Create first node source and choose file with options enabled: generate and include server node
 - Create another node source and choose script [nodes.sh](rundeck/nodes.sh), format:  resourcexml, script path `/var/rundeck/scripts/automation/nodes.sh`
4. To save some system resources (in production use) change node cache delay to some reasonable number, for example 300 seconds or more (depends on number of hosts defined in Zabbix)
5. Edit SQL select in [nodes.sh](rundeck/nodes.sh) based on your requirements (I'm using Asset tag = AUTO to enable automation)

### Zabbix server

#### Postfix setup

Postfix here is used for trigger action "queue". In Zabbix is not so easy handle actions if trigger action fails. With postfix, email is returned back to sender address, so you can just monitor this mailbox. Also you can choose own retry delay and retry count specify in script.

1. Update postfix configuration: `/etc/postfix/main.cf`
 - Redeliver bounced email every 30 seconds (you can change this value)  
   `queue_run_delay = 15s`
   `minimal_backoff_time = 30s`
   `maximal_backoff_time = 30s`
 - I'm using local postfix only for Zabbix purpose, so changing `nobody` to `zabbix` user (if you choose different user, change different folders or change ownership for folders defined in [event2rundeck.sh](zabbix/event2rundeck.sh)  
   `default_privs = zabbix`
 - Append your domain for local delivery in postfix, feel free to change this domain  
   `mydestination = $myhostname, localhost.$mydomain, localhost, rundeck.local`

2. Edit mail alias configuration in `/etc/aliases`  
   ```
   # Zabbix to Rundeck projects (automation project here), mail is sent to automation@rundeck.local 
   automation:     |/usr/local/bin/event2rundeck.sh
   ```

3. Copy script [event2rundeck.sh](zabbix/event2rundeck.sh) to `/usr/local/bin/event2rundeck.sh`
4. Reload aliases and restart postfix
   ```
   $ sudo newaliases 
   $ sudo systemctl restart postfix.service
   ```

#### Script update

1. If `zabbix` is not postfix `default_privs`, then change folders or privileges for:
   ```
   workdir=/var/log/zabbix/rdevents
   logfile=/var/log/zabbix/rundeck.log
   ```
   
2. Update variable for Rundeck:
   ```
   rdurl="http://127.0.0.1:4440"	# Rundeck URL
   rdapi=23							# Rundeck API version
   rdtoken=XYZ						# Rundeck auth token
   ```

Rest of script can stay default, but feel free to change maxretry, timeout, ... Comments inside script should help.

#### Zabbix inventory

Required values to fill for every host are `Asset tag` and `Installer name`.
- `Asset tag` is mentioned in Rundeck server part
- `Installer name` is used for Rundeck node user definition

Rest is optional, but can be useful for different purpose jobs.  

- Zabbix inventory  
![Zabbix inventory](images/zabbix-inventory.png)  
- Rundeck inventory  
![Rundeck inventory](images/rundeck-inventory.png)  

#### Zabbix template

In Configuration &rarr; Template choose import and load [automation_template.xml](zabbix/automation_template.xml).  
Following options must be selected in `Create new` section:
- Groups
- Templates
- Applications
- Items
- Triggers

When done, assign host(s) with properly filled inventory data to template `Template Automation Linux`

#### Zabbix media type and user
1. Create new media type in Administration &rarr; Media types
 - Name: `Automation`
 - Type: `Email`
 - SMTP server: `localhost`
 - SMTP server port: `25`
 - SMTP email: `zabbix@localhost` (mail address where you want to return undelivered/returned notifications)

2. Create new user in Administration &rarr; Users. Fill whatever you want, but add new media in  Media tab:
 - Type: `Automation`
 - Send to: `automation@rundeck.local`

#### Zabbix action

In Configuration &rarr; Actions add new Action.  

1\. Action:
 - Name: `Rundeck Automation`
 - Type of calculation: `And`
 - Conditions: `Maintenance status not in maintenance` And `Template = Template Automation Linux`

2\. Operations (don't modify those values, otherwise it will not work):
 - Default subject: `{HOST.NAME}:{TRIGGER.NAME}:{TRIGGER.NSEVERITY}:{EVENT.ID}`
 - Default message:

```
Problem started at {EVENT.TIME} on {EVENT.DATE}

Problem name: {TRIGGER.NAME}
Host: {HOST.NAME}
Severity: {TRIGGER.SEVERITY}
Trigger status: {TRIGGER.STATUS}

Additional info: {ITEM.KEY1}:{ITEM.VALUE1}

Original problem ID: {EVENT.ID}
```
 - Operations:
  - Operation type `Send message`
  - Send to Users `youruser` (created in previous step)
  - Send only to `Automation`   
   
3\. Recovery operations (don't modify those values, otherwise it will not work):
 - Default subject: `{HOST.NAME}:{TRIGGER.NAME}:{TRIGGER.NSEVERITY}:{EVENT.ID}`
 - Default message:

```
Problem has been resolved at {EVENT.RECOVERY.TIME} on {EVENT.RECOVERY.DATE}

Problem name: {TRIGGER.NAME}
Host: {HOST.NAME}
Severity: {TRIGGER.SEVERITY}
Trigger status: {TRIGGER.STATUS}

Additional info: {ITEM.KEY1}:{ITEM.VALUE1}

Original problem ID: {EVENT.ID}
Recovery problem ID: {EVENT.RECOVERY.ID}
```  
 - Operations:
  - Operation type `Send message`
  - Send to Users `youruser` (created in previous step)
  - Send only to `Automation`  

## Testing

This is all, so let's try how all those tools works great together!

1. On host with Automation template assigned, trigger event where trigger name match Rundeck job name:  
   `$ rm -f /tmp/file.create`  
   or  
   `$ touch /tmp/file.remove`  
2. On host with Automation template assigned, trigger event where trigger name doesn't match Rundeck job name:  
    Create script `killme.sh`  

    ```
    #!/bin/bash
    while true; do
     date
     sleep 30
    done
    ```  
    Run the script `nohup ./killme.sh 2>&1 &`  
3. Now if everything has been configured properly you should see Zabbix events triggering Rundeck jobs and Rundeck jobs creating/updating/resolving tickets on OTRS.  


----------  


