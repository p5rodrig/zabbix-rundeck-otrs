#!/bin/bash

## Set max retry and delay if not defined
[ -z $RD_OPTION_MAXRETRY ] && RD_OPTION_MAXRETRY=0	# No retry if not defined
[ -z $RD_OPTION_DELAY ] && RD_OPTION_DELAY=30		# Default retry delay 30s
let retry=0

## Pass TicketID to script
RD_DATA_TICKETID=$1
[ -z $RD_DATA_TICKETID ] && RD_DATA_TICKETID=NULL

## Retry function
checkretry () {
	[ $1 = OK ] && echo "$action $1: $retry/$RD_OPTION_MAXRETRY."
	# Never kill execution if Zabbix ack failed
	[ $retry -eq $RD_OPTION_MAXRETRY ] && echo "$action $1, max retry reached: $retry/$RD_OPTION_MAXRETRY" && break
	[ $1 = FAILED ] && echo "$action FAILED: $retry/$RD_OPTION_MAXRETRY!"
}

## Get Zabbix auth token
gettoken () {
	token=$(curl -s -X POST -H "Content-Type: application/json-rpc" "$RD_OPTION_ZABBIXURL/api_jsonrpc.php" -d \
	"{
		\"jsonrpc\": \"2.0\",
		\"method\": \"user.login\",
		\"params\": {
		\"user\": \"$RD_OPTION_ZABBIXUSR\",
		\"password\": \"$RD_OPTION_ZABBIXPW\"
	},
	\"id\": 1
	}" | jq '.result')

	action="Zabbix login"
	if [[ $token =~ ^\"?[0-9a-f]+\"?$ ]]; then
		tokenresult=OK
		checkretry OK
	else
		checkretry FAILED
	fi
}

## Acknowledge Zabbix event
sendack () {
	ackevent=$(curl -s -X POST -H "Content-Type: application/json-rpc" "$RD_OPTION_ZABBIXURL/api_jsonrpc.php" -d \
	"{
		\"jsonrpc\": \"2.0\",
		\"method\": \"event.acknowledge\",
		\"params\": {
			\"eventids\": \"$RD_OPTION_EVENTID\",
			\"message\": \"ExecutionID=$RD_JOB_EXECID,TicketID=$RD_DATA_TICKETID\",
			\"action\": 0
		},
	\"auth\": $token,
	\"id\": 1
	}" | jq '.result.eventids[0]')

	action="Zabbix event ackowledment"
	if [[ $ackevent =~ ^\"?[0-9]+\"?$ ]]; then
		ackresult=OK 	# prevent ack loop
		checkretry OK
	else
		checkretry FAILED
	fi
}

## Zabbix logout
sendlogout () {
	logout=$(curl -s -X POST -H "Content-Type: application/json-rpc" "$RD_OPTION_ZABBIXURL" -d \
	"{
		\"jsonrpc\": \"2.0\",
		\"method\": \"user.logout\",
		\"params\": [],
		\"id\": 1,
		\"auth\": $token
	}")
}

### Main script execution
while [ $retry -le $RD_OPTION_MAXRETRY ]; do
	[[ $ackresult != OK ]] && gettoken
	[[ $tokenresult = OK ]] && [[ $ackresult != OK ]] && sendack
	[[ $tokenresult = OK ]] && [[ $ackresult = OK ]] && sendlogout && break
	let retry=$retry+1
	sleep $RD_OPTION_DELAY
done

exit 0
