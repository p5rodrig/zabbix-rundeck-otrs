#!/bin/bash

## Set max retry and delay if not defined
[ -z $RD_OPTION_MAXRETRY ] && RD_OPTION_MAXRETRY=0      # No retry if not defined
[ -z $RD_OPTION_DELAY ] && RD_OPTION_DELAY=30           # Default retry delay 30s
let retry=0

## Create OTRS ticket function
createticket () {
	ticketid=$(curl -s -X POST "$RD_OPTION_OTRSURL/nph-genericinterface.pl/Webservice/REST/Ticket?CustomerUserLogin=$RD_OPTION_OTRSUSR&Password=$RD_OPTION_OTRSPW" -d \
	"{
	\"Ticket\":
		{
		\"Title\":\"$RD_OPTION_SUBJECT\",
		\"Type\": \"Unclassified\",
		\"Queue\":\"Automation\",
		\"State\":\"new\",
		\"PriorityID\":\"$RD_OPTION_SEVERITY\",
		\"CustomerUser\":\"$RD_OPTION_OTRSUSR\"
		},
	\"Article\":
		{
		\"Subject\":\"Automation create ticket\",
		\"Body\":\"$message\n\nExecution link: $RD_JOB_URL\",
		\"ContentType\":\"text/plain; charset=utf8\"
		}
	}" | jq '.TicketID' | tr -d '"')
}

## Convert message new lines to \n
message=$(echo "$RD_OPTION_MESSAGE" | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g')

while [ $retry -le $RD_OPTION_MAXRETRY ]; do
	createticket
	action="Create OTRS ticket"
	if [[ $ticketid =~ ^[0-9]+$ ]]; then
		echo "$action OK: $retry/$RD_OPTION_MAXRETRY."
		echo "RUNDECK:DATA:ticketid = $ticketid"
		break
	else
		# No execution without OTRS ticket
		[ $retry -eq $RD_OPTION_MAXRETRY ] && echo "$action FAILED, max retry reached: $retry/$RD_OPTION_MAXRETRY!" && exit 1
		echo "$action FAILED: $retry/$RD_OPTION_MAXRETRY!"
		let retry=$retry+1
		sleep $RD_OPTION_DELAY
	fi
done

exit 0
